import { call, put, takeLatest } from 'redux-saga/effects';
import {
  updateTodosFailure,
  updateTodosStart,
  updateTodosSuccess,
} from '../slices/todoSlice';
import { showSnackbar } from '../slices/snackBarSlice';
import { updateTodos } from '../../services/updateTodosService';

export function* onUpdatetodo() {
  yield takeLatest(updateTodosStart.type, workUpdateTodos);
}

function* workUpdateTodos({ payload }) {
  const { id, title } = payload;
  const todo = {
    userId: 1,
    title,
    completed: false,
    id,
  };

  try {
    const { status, data } = yield call(updateTodos, id, todo);
    if (status === 200) {
      yield put(updateTodosSuccess(data));
      yield put(showSnackbar('Updated successfully '));
    }
  } catch (error) {
    if (error.response.status === 500) {
      yield put(updateTodosSuccess(todo));
      yield put(showSnackbar('Updated successfully '));
      console.log(
        `error on server side ${error.message} but successfully updated locally`,
      );
    } else {
      yield put(updateTodosFailure(error.message));
      yield put(showSnackbar(error.message));
    }
  }
}
