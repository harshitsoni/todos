import { call, put, takeEvery } from 'redux-saga/effects';
import {
  getTodosFailure,
  getTodosStart,
  getTodosSuccess,
} from '../slices/todoSlice';
import { fetchTodos } from '../../services/fetchTodosService';
import { showSnackbar } from '../slices/snackBarSlice';

export function* onLoadtodos() {
  yield takeEvery(getTodosStart.type, workGetTodos);
}

function* workGetTodos() {
  try {
    const { status, data } = yield call(fetchTodos);
    if (status === 200) yield put(getTodosSuccess(data));
  } catch (error) {
    yield put(getTodosFailure(error.message));
    yield put(showSnackbar(error.message));
  }
}
