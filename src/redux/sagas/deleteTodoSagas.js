import { call, put, takeLatest } from 'redux-saga/effects';
import {
  addTodosFailure,
  deleteTodosStart,
  deleteTodosSuccess,
} from '../slices/todoSlice';
import { deleteTodos } from '../../services/deleteTodosService';
import { showSnackbar } from '../slices/snackBarSlice';

export function* onDeletetodo() {
  yield takeLatest(deleteTodosStart.type, workDeleteTodos);
}

function* workDeleteTodos({ payload: id }) {
  try {
    const { status } = yield call(deleteTodos, id);
    if (status === 200) {
      yield put(deleteTodosSuccess(id));
      yield put(showSnackbar('Deleted successfully '));
    }
  } catch (error) {
    yield put(addTodosFailure(error.message));
    yield put(showSnackbar(error.message));
  }
}
