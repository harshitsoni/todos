import { call, put, takeLatest } from 'redux-saga/effects';
import { showSnackbar } from '../slices/snackBarSlice';
import {
  addTodosFailure,
  addTodosStart,
  addTodosSuccess,
} from '../slices/todoSlice';
import { addTodos } from '../../services/addTodosService';

export function* onAddtodo() {
  yield takeLatest(addTodosStart.type, workAddTodos);
}

function* workAddTodos({ payload }) {
  try {
    const { status, data } = yield call(addTodos, payload);
    if (status === 201) {
      yield put(addTodosSuccess(data));
      yield put(showSnackbar('Added successfully '));
    }
  } catch (error) {
    yield put(addTodosFailure(error.message));
    yield put(showSnackbar(error.message));
  }
}
