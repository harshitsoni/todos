import { all, fork } from 'redux-saga/effects';
import { onLoadtodos } from './loadTodoSagas';
import { onAddtodo } from './addTodoSagas';
import { onDeletetodo } from './deleteTodoSagas';
import { onUpdatetodo } from './updateTodoSagas';

const todoSagas = [
  fork(onLoadtodos),
  fork(onAddtodo),
  fork(onDeletetodo),
  fork(onUpdatetodo),
];

export default function* rootSaga() {
  yield all([...todoSagas]);
}
