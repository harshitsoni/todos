import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isSnackbarOpen: false,
  snackbarMessage: '',
};

export const snackbarSlice = createSlice({
  name: 'snackbar',
  initialState,
  reducers: {
    showSnackbar: (state, action) => {
      state.isSnackbarOpen = true;
      state.snackbarMessage = action.payload;
    },
    closeSnackbar: (state) => {
      state.isSnackbarOpen = false;
      state.snackbarMessage = '';
    },
  },
});

export const { showSnackbar, closeSnackbar } = snackbarSlice.actions;

export default snackbarSlice.reducer;
