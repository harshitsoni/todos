import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  latestTodo: {},
  todoList: [],
  isLoading: false,
  error: '',
};

export const todoSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    getTodosStart: (state) => {
      state.isLoading = true;
    },
    getTodosSuccess: (state, action) => {
      state.todoList = action.payload;
      state.latestTodo = state.todoList[state.todoList.length - 1];
      state.isLoading = false;
    },
    getTodosFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    addTodosStart: (state) => {
      state.isLoading = true;
    },
    addTodosSuccess: (state, action) => {
      const newId = state.latestTodo.id + 1;
      action.payload.id = newId;
      state.latestTodo = action.payload;
      state.todoList.unshift(action.payload);
      state.isLoading = false;
    },
    addTodosFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    deleteTodosStart: (state) => {
      state.isLoading = true;
    },
    deleteTodosSuccess: (state, action) => {
      state.todoList = state.todoList.filter(
        (item) => item.id !== action.payload,
      );
      state.isLoading = false;
    },
    deleteTodosFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    updateTodosStart: (state) => {
      state.isLoading = true;
    },
    updateTodosSuccess: (state, action) => {
      const todo = action.payload;
      const updatedItemIndex = state.todoList.findIndex(
        (item) => item.id === todo.id,
      );
      if (updatedItemIndex !== -1) {
        state.todoList[updatedItemIndex] = todo;
      }
      state.isLoading = false;
    },
    updateTodosFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
  },
});

export const {
  getTodosStart,
  getTodosSuccess,
  getTodosFailure,
  addTodosStart,
  addTodosSuccess,
  addTodosFailure,
  deleteTodosStart,
  deleteTodosSuccess,
  deleteTodosFailure,
  updateTodosStart,
  updateTodosSuccess,
  updateTodosFailure,
} = todoSlice.actions;

export default todoSlice.reducer;
