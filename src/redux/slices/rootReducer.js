import { combineReducers } from 'redux';
import TodoReducer from './todoSlice';
import snackBarReducer from './snackBarSlice';

const rootReducer = combineReducers({
  todos: TodoReducer,
  snackbar: snackBarReducer,
});

export default rootReducer;
