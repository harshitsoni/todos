import axios from 'axios';
import constants from '../config/constants';

export const deleteTodos = async (id) =>
  await axios.delete(`${constants.API_ENDPOINT}/${id}`);
