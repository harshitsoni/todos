import axios from 'axios';
import constants from '../config/constants';

export const updateTodos = async (id, todo) =>
  await axios.put(`${constants.API_ENDPOINT}/${id}`, todo);
