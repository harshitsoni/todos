import axios from 'axios';
import constants from '../config/constants';

export const addTodos = async (todo) =>
  await axios.post(constants.API_ENDPOINT, todo);
