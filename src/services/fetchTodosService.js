import axios from 'axios';
import constants from '../config/constants';

export const fetchTodos = async () => await axios.get(constants.API_ENDPOINT);
