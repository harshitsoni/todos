const API_ENDPOINT = 'https://jsonplaceholder.typicode.com/todos';
const POLICY_URL = 'https://www.gammastack.com/privacy-policy/';

const constants = {
  API_ENDPOINT,
  POLICY_URL,
};

export default constants;
