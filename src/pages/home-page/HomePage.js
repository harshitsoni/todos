import React from 'react';
import Header from '../../ui-kit/components/Header';
import Footer from '../../ui-kit/components/Footer';
import CreateTodos from './components/CreateTodos';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getTodosStart } from '../../redux/slices/todoSlice';
import TodosList from './components/TodosList';

const HomePage = () => {
  const todosList = useSelector((state) => state.todos.todoList);
  const dispatch = useDispatch();

  const [editInfo, setEditInfo] = useState({});

  useEffect(() => {
    dispatch(getTodosStart());
  }, [dispatch]);

  const editHandler = (editId, editTitle) => {
    const EditInfo = {
      editId,
      editTitle,
    };
    setEditInfo(EditInfo);
  };

  const editCloseHandler = () => {
    setEditInfo({});
  };

  return (
    <>
      <Header />
      <main>
        <CreateTodos editInfo={editInfo} onUpdate={editCloseHandler} />
        <TodosList todos={todosList} editInfo={editInfo} onEdit={editHandler} />
      </main>
      <Footer />
    </>
  );
};

export default HomePage;
