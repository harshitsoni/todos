import React from 'react';
import { deleteTodosStart } from '../../../redux/slices/todoSlice';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {
  Button,
  ButtonGroup,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

const TodosList = ({ todos, onEdit, editInfo }) => {
  const dispatch = useDispatch();

  const { editId } = editInfo;

  return (
    <>
      <Grid
        container
        spacing={4}
        direction="column"
        alignItems="center"
        justifyContent="center"
        sx={{ py: 8 }}
      >
        <Grid item xs={3}>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>
                  <Typography variant="subtitle2">Id</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2">Title</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2">Actions</Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {todos.map((item) => (
                <TableRow key={item.id}>
                  <TableCell>{item.id}</TableCell>
                  <TableCell>{item.title}</TableCell>
                  <TableCell>
                    <ButtonGroup
                      size="medium"
                      variant="contained"
                      aria-label="outlined primary button group"
                    >
                      <Button
                        onClick={() => {
                          onEdit(item.id, item.title);
                        }}
                        startIcon={<EditIcon />}
                      >
                        Edit
                      </Button>
                      <Button
                        disabled={editId && editId === item.id}
                        onClick={() => {
                          dispatch(deleteTodosStart(item.id));
                        }}
                        variant="outlined"
                        startIcon={<DeleteIcon />}
                      >
                        Delete
                      </Button>
                    </ButtonGroup>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Grid>
      </Grid>
    </>
  );
};

TodosList.propTypes = {
  todos: PropTypes.array,
  onEdit: PropTypes.func,
  editInfo: PropTypes.object,
};

export default TodosList;
