import React, { useState, useRef, useEffect } from 'react';
import Box from '@mui/material/Box';
import {
  Button,
  CircularProgress,
  Grid,
  Stack,
  TextField,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import {
  addTodosStart,
  updateTodosStart,
} from '../../../redux/slices/todoSlice';
import AddIcon from '@mui/icons-material/Add';
import PropTypes from 'prop-types';

const CreateTodos = ({ editInfo, onUpdate }) => {
  const { editId, editTitle } = editInfo;
  const [newTitle, setNewTitle] = useState(editTitle || '');
  const isLoading = useSelector((state) => state.todos.isLoading);
  const dispatch = useDispatch();
  const inputRef = useRef(null);

  useEffect(() => {
    if (editId && inputRef.current) {
      inputRef.current.focus();
    }
    setNewTitle(editTitle);
  }, [editId, editTitle]);

  const addTodoHandler = (event) => {
    event.preventDefault();
    if (newTitle.trim().length !== 0) {
      const newTodo = {
        userId: 1,
        title: newTitle,
        completed: false,
      };
      dispatch(addTodosStart(newTodo));
    }
    setNewTitle('');
  };

  const updateTodoHandler = (event) => {
    event.preventDefault();
    if (newTitle.trim().length !== 0) {
      dispatch(updateTodosStart({ id: editId, title: newTitle }));
    }
    setNewTitle('');
    onUpdate();
  };

  return (
    <>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        sx={{ pt: 8 }}
      >
        <Grid item xs={3}>
          <Box component="form">
            <Stack spacing={2} direction="row">
              <TextField
                margin="normal"
                required
                id="todo-title"
                label={!editId ? 'title' : 'Enter New title'}
                name="todo-title"
                autoFocus
                inputRef={inputRef}
                value={newTitle}
                onChange={(e) => setNewTitle(e.target.value)}
              />
              {isLoading ? (
                <CircularProgress />
              ) : editId ? (
                <Button
                  type="submit"
                  variant="contained"
                  onClick={updateTodoHandler}
                >
                  Update
                </Button>
              ) : (
                <Button
                  type="submit"
                  variant="contained"
                  onClick={addTodoHandler}
                  startIcon={<AddIcon />}
                >
                  Add
                </Button>
              )}
            </Stack>
          </Box>
        </Grid>
      </Grid>
    </>
  );
};

CreateTodos.propTypes = {
  onUpdate: PropTypes.func,
  editInfo: PropTypes.object,
};

export default CreateTodos;
