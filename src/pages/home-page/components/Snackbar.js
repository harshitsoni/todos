import { Snackbar as MUISnackBar } from '@mui/material';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { closeSnackbar } from '../../../redux/slices/snackBarSlice';

const Snackbar = () => {
  const dispatch = useDispatch();
  const { isSnackbarOpen, snackbarMessage } = useSelector(
    (state) => state.snackbar,
  );

  return (
    <MUISnackBar
      open={isSnackbarOpen}
      autoHideDuration={5000}
      onClose={() => {
        dispatch(closeSnackbar());
      }}
      message={snackbarMessage}
    />
  );
};

export default Snackbar;
