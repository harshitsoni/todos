const primary = '#556cd6';
const secondary = '#19857b';

const colors = { primary, secondary };

export default colors;
