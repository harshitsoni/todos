import { Link, Typography } from '@mui/material';
import React from 'react';
import constants from '../../config/constants';

const Copyright = () => {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      Copyright 2023
      <Link color="inherit" href={constants.POLICY_URL}>
        | Privacy Policy |
      </Link>
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
};

export default Copyright;
