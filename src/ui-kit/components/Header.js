import React from 'react';
import { AppBar, Stack, Toolbar, Typography } from '@mui/material';
import ListAltIcon from '@mui/icons-material/ListAlt';
const Header = () => {
  return (
    <>
      <AppBar position="relative">
        <Toolbar>
          <Stack spacing={1} direction="row">
            <ListAltIcon />
            <Typography variant="h6" color="inherit" noWrap>
              Todos
            </Typography>
          </Stack>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
