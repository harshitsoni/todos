import { Box, Typography } from '@mui/material';
import React from 'react';
import Copyright from './Copyright';

const Footer = () => {
  return (
    <>
      <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
        <Typography variant="h6" align="center" gutterBottom>
          GammaStack
        </Typography>
        <Typography
          variant="subtitle1"
          align="center"
          color="text.secondary"
          component="p"
        >
          GammaStack is an end–to–end IT services and solutions provider
          delivering platforms that overcome business challenges and increase
          revenues. Our services revolve around software product development
          using multiple technologies to ensure quick time to market, quality
          and peace of mind. 200+ team members determined to serve nothing but
          the best to our clients, we offer a plethora of customized software
          development services accompanied with out-of-box approach, adeptness
          and growth innovation strategies that are well directed towards
          achieving high-end results.
        </Typography>
        <Copyright />
      </Box>
    </>
  );
};

export default Footer;
