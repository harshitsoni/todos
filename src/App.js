import * as React from 'react';
import HomePage from './pages/home-page/HomePage';
import { Snackbar } from '@mui/material';

export default function App() {
  return (
    <>
      <HomePage />
      <Snackbar />
    </>
  );
}
